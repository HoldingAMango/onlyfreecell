﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FreeCellSlot : MonoBehaviour, IDropHandler {

	public string _suit = "";
	int _lastValue = 0;

	FreeCellGameManager _manager;

	public void SetGameManager(FreeCellGameManager manager){
		_manager = manager;
	}

	public void OnDrop (PointerEventData eventData){
		DraggableCard card = eventData.pointerDrag.GetComponent<DraggableCard> ();

		if (card.GetCardValue()==_lastValue+1 && card.GetCardSuite()==_suit && card.HasLinkedCard()==false) {
			card.transform.SetParent (this.transform);
			card.SetLastParent (this.transform);
			card.SetCardInSlot (Vector2.zero);
			card.DropCard ();
			card.SetCanPickup(false);
			card.MoveToTopZOrder ();
			_lastValue = _lastValue + 1;
			card.ReAssignCard ();

			if (_lastValue == 13) {
				_manager.UpdateWinConditionToTrue (_suit);
			}
		}
	}

	public void ResetGame(){
		foreach (Transform child in transform) {
			GameObject.Destroy(child.gameObject);
		}
		_lastValue = 0;
	}

	public void CheckPlacable(DraggableCard card){
		if (card.GetCardValue()==_lastValue+1 && card.GetCardSuite()==_suit && card.HasLinkedCard()==false) {
			card.transform.SetParent (this.transform);
			card.SetLastParent (this.transform);
			card.SetCardInSlot (Vector2.zero);
			card.DropCard ();
			card.SetCanPickup(false);
			card.MoveToTopZOrder ();
			_lastValue = _lastValue + 1;
			card.ReAssignCard ();
		}
	}
}
