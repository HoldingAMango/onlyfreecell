﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class DraggableCard: MonoBehaviour, IDragHandler, IPointerDownHandler, IPointerUpHandler {

	//Design Items
	private RectTransform _dragRecTx;
	public Canvas _canvas;
	public Text _cardLabel;


	//Card data
	CardData _data;

	//UI control states
	bool pointerDown = false;
	public bool _canPickup = true;
	Vector2 _lastposition = Vector2.zero;
	Transform _lastparent;
	bool hasDragged = false;

	public System.Action<DraggableCard> reAssignedEvent = (c) => {};
	public System.Action<DraggableCard> onClickEvent = (c) => {};

	//private bool for controlling dropping and picking up cards
	bool _isDraggable = true;

	DraggableCard _linkedCard;

	void Awake(){
		if (_dragRecTx == null) {
			_dragRecTx = transform.gameObject.GetComponent<RectTransform> ();
		}
	}

	void Start(){
		if (_data == null) {
			CardData test = new CardData ("heart", 3);
			SetData (test);
		}
	}

	public void SetData(CardData data){
		_data = data;
		_cardLabel.text = _data.GetCardName ();
	}

	public void SetCanvasRef(Canvas newcanvas){
		_canvas = newcanvas;
	}

	public string GetCardSuite(){
		return _data.GetSuite ();
	}

	public int GetCardValue(){
		return _data.GetValue ();
	}

	public void SetCanPickup(bool flag){
		_canPickup = flag;
	}
		
	public bool HasLinkedCard(){
		return _linkedCard != null;
	}

	public void RecursiveAddCards(CardPositionSlot slot){
		if (HasLinkedCard ()) {
			slot.RecursiveAdd (_linkedCard);
			_linkedCard.RecursiveAddCards (slot);
		}
	}

	public void SetLinkedCard(DraggableCard _next){
		_linkedCard = _next;
	}

	public void UnSetLinkedCard(){
		_linkedCard = null;
	}

	public bool CheckCanPickup(){

		if (HasLinkedCard ()) {
			return _linkedCard.CheckCanPickup ();
		}
		return _canPickup;
	}

	public void OnDrag(PointerEventData ptEvent){
		if (CheckCanPickup() == false)
			return;
		DoDrag (ptEvent);
		hasDragged = true;
	}

	public void DoDrag(PointerEventData ptEvent){
		_dragRecTx.anchoredPosition += ptEvent.delta / _canvas.scaleFactor;
		_dragRecTx.SetAsLastSibling ();
		if (_linkedCard != null) {
			_linkedCard.DoDrag (ptEvent);
		}
	}

	public void DropCard(){
		_isDraggable = false;
		GetComponent<CanvasGroup> ().blocksRaycasts = true;
	}

	public void OnPointerDown(PointerEventData ptEvent){
		if (CheckCanPickup() == false)
			return;
		
		if (transform.parent != _canvas)
			_lastparent = transform.parent;

		_lastposition = _dragRecTx.anchoredPosition;
		transform.SetParent (_canvas.gameObject.transform);

		_dragRecTx.SetAsLastSibling ();
		pointerDown = true;
		//_isDraggable = true;
		GetComponent<CanvasGroup> ().blocksRaycasts = false;
		hasDragged = false;

		if (HasLinkedCard()) {
			_linkedCard.RecursiveSetPointerDown (ptEvent);
		}
	}

	public void RecursiveSetPointerDown(PointerEventData ptEvent){
		if (transform.parent != _canvas)
			_lastparent = transform.parent;

		_lastposition = _dragRecTx.anchoredPosition;
		transform.SetParent (_canvas.gameObject.transform);
		_dragRecTx.SetAsLastSibling ();
		pointerDown = true;
		if (HasLinkedCard ()) {
			_linkedCard.RecursiveSetPointerDown (ptEvent);
		}
	}

	public Transform GetLastParent(){
		return _lastparent;
	}

	public void SetLastParent(Transform newparent){
		_lastparent = newparent;
	}

	public void MoveToTopZOrder(){
		_dragRecTx.SetAsLastSibling ();
	}

	public void SetCardInSlot(Vector2 v){
		_dragRecTx.anchoredPosition = v;
		_lastposition = v;
	}

	public void ReAssignCard(){
		reAssignedEvent (this);
	}

	public void OnPointerUp(PointerEventData ptEvent){
		if (CheckCanPickup() == false)
			return;
		if (_lastparent != null)
			transform.SetParent (_lastparent);
		_dragRecTx.anchoredPosition = _lastposition;
		pointerDown = false;
		GetComponent<CanvasGroup> ().blocksRaycasts = true;
		if(hasDragged==false){
			onClickEvent(this);
		}
		if (HasLinkedCard ()) {
			_linkedCard.RecursivePointerUp (ptEvent);
		}
	}

	public void RecursivePointerUp(PointerEventData ptEvent){
		if (_lastparent != null)
			transform.SetParent (_lastparent);
		_dragRecTx.anchoredPosition = _lastposition;
		pointerDown = false;
		GetComponent<CanvasGroup> ().blocksRaycasts = true;
		if (HasLinkedCard ()) {
			_linkedCard.RecursivePointerUp (ptEvent);
		}
	}

	//place holder
	void Update(){
	}
}
