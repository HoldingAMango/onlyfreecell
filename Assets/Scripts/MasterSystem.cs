﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MasterSystem : MonoBehaviour {

	public GameObject gameWindow;
	public GameObject welcomeWindow;
	public GameObject endWindow;

	public void WinGame(){
//		FreeCellGameManager gameManager = gameWindow.GetComponent<FreeCellGameManager> ();
		welcomeWindow.SetActive(false);
		gameWindow.SetActive(false);
		endWindow.SetActive(true);
	}

	public void StartGame(){
		welcomeWindow.SetActive(false);
		gameWindow.SetActive(true);
		endWindow.SetActive(false);

		FreeCellGameManager gameManager = gameWindow.GetComponent<FreeCellGameManager> ();
		gameManager.ResetGame ();
	}

	public void GotoMainMenu(){
		welcomeWindow.SetActive(true);
		gameWindow.SetActive(false);
		endWindow.SetActive(false);
	}

}
