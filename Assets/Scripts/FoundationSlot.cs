﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class FoundationSlot : MonoBehaviour, IDropHandler {

	public GameObject card {
		get {
			if (transform.childCount > 0) {
				return transform.GetChild (0).gameObject;
			} else {
				return null;
			}
		}
	}

	public void OnDrop (PointerEventData eventData){
		if (card ==null) {
			DraggableCard card = eventData.pointerDrag.GetComponent<DraggableCard> ();
			if (card._canPickup == false || card.HasLinkedCard()==true)
				return;
			card.transform.SetParent (this.transform);
			card.SetLastParent (this.transform);
			card.SetCardInSlot (Vector2.zero);
			card.DropCard ();
			card.ReAssignCard ();
		}
	}

	public void ResetGame(){
		foreach (Transform child in transform) {
			GameObject.Destroy(child.gameObject);
		}
	}
}
