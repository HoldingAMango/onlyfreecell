﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CardPositionSlot : MonoBehaviour, IDropHandler {

	//design
	public GameObject cardPrefab;
	public Canvas _canvas;

	//States
	int _lastValue = 0;
	string _lastsuite = "";
	List<DraggableCard> cardList = new List<DraggableCard>();
	FreeCellGameManager _manager;

	void Start(){
//		AddCard (new CardData ("heart", 1));
//		AddCard (new CardData ("heart", 3));
//		AddCard (new CardData ("heart", 4));
//		AddCard (new CardData ("heart", 1));
//		AddCard (new CardData ("heart", 3));
//		AddCard (new CardData ("heart", 4));
//		AddCard (new CardData ("heart", 2));
	}

	public void SetGameManager(FreeCellGameManager manager){
		_manager = manager;
	}

	public void ResetGame(){
		foreach (Transform child in transform) {
			GameObject.Destroy(child.gameObject);
		}
		_lastValue = 0;
		_lastsuite = "";
		cardList = new List<DraggableCard>();

	}

	public void AddCard(CardData carddata){
		GameObject cardObj = Instantiate (cardPrefab);
		DraggableCard new_card = cardObj.GetComponent<DraggableCard> ();
		new_card.SetCanvasRef(_canvas);
		new_card.SetData (carddata);
		AddToSlotUI (new_card);
		cardList.Add (new_card);
		RefreshCardList();
	}

	void RefreshCardList(){
		_lastsuite = "any";
		List<DraggableCard> removebin = new List<DraggableCard> ();
		int y = 0;
		DraggableCard lastcard = null;
		foreach (DraggableCard card in cardList){
			if (card.GetLastParent()!= this.transform) {
				Debug.Log("removing a card");
				removebin.Add (card);
			}else{
				card.MoveToTopZOrder();
				card.SetCardInSlot(new Vector2(0,y));
				_lastValue = card.GetCardValue();
				_lastsuite = card.GetCardSuite ();
				Debug.Log (y);
				y -= 20;
				if (lastcard != null) {
					if (lastcard.GetCardSuite () == _lastsuite && lastcard.GetCardValue () == _lastValue + 1) {
						lastcard.SetLinkedCard (card);
					} else {
						lastcard.SetCanPickup (false);
					}
				}
				lastcard = card;
				lastcard.SetCanPickup (true);
				lastcard.SetLinkedCard (null);
			}
		}
		foreach (DraggableCard removeCard in removebin) {
			cardList.Remove (removeCard);
		}
	}

	public void OnDrop (PointerEventData eventData){
		DraggableCard card = eventData.pointerDrag.GetComponent<DraggableCard> ();

		if ((cardList.Count<8 && card.GetCardValue()==_lastValue-1 && card.GetCardSuite()==_lastsuite) || _lastsuite == "any") {
			AddToSlotUI (card);
			cardList.Add (card);
			if (card.HasLinkedCard () == true) {
				card.RecursiveAddCards (this);
			}
			RefreshCardList ();
		}
	}

	public void RecursiveAdd(DraggableCard card){
		AddToSlotUI (card);
		cardList.Add (card);
		RefreshCardList ();
	}

	public void AddToSlotUI(DraggableCard card){
		card.transform.SetParent (this.transform);
		card.SetLastParent (this.transform);
		card.SetCardInSlot (Vector2.zero);
		card.DropCard ();
		card.SetCanPickup(false);
		card.MoveToTopZOrder ();
		card.ReAssignCard ();
		card.reAssignedEvent += ReAssignHandler;
		card.onClickEvent += OnClickHandler;
	}

	public void ReAssignHandler(DraggableCard card){
		cardList.Remove (card);
		card.reAssignedEvent -= ReAssignHandler;
		card.onClickEvent -= OnClickHandler;
		RefreshCardList ();
	}

	public void OnClickHandler(DraggableCard card){
		_manager.FindSuitableFreeCell (card);
	}
}
