﻿using System.Collections;
using System.Collections.Generic;

public class CardData {

	//place holder variables - change to enum
	string _cardsuite = "";
	int _cardvalue = 0;

	public CardData(string suite, int value){
		_cardsuite = suite;
		_cardvalue = value;
	}

	public string GetSuite(){
		return _cardsuite;
	}

	public string GetCardName(){
		string[] cardNames = {"0","A","2","3","4","5","6","7","8","9","10","J","Q","K"};
		return cardNames [_cardvalue] + " "+ _cardsuite;
	}

	public int GetValue(){
		return _cardvalue;
	}
}
