﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class FreeCellGameManager : MonoBehaviour {

	public List<FoundationSlot> foundationpositions = new List<FoundationSlot>();
	public List<FreeCellSlot> freecellpositions = new List<FreeCellSlot> ();
	public List<CardPositionSlot> positions = new List<CardPositionSlot>();

	public Dictionary<string,bool> winConditions = new Dictionary<string,bool> ();
	string[] suitMap = {"hearts","clover","clubs","diamond"};

	MasterSystem _masterSys;

	void Start(){
		foreach (CardPositionSlot pos in positions) {
			pos.SetGameManager(this);
		}
	}

	public void SetMainSystem(MasterSystem main_system){
		_masterSys = main_system;
	}


	public void ResetGame(){
		foreach (CardPositionSlot pos in positions) {
			pos.ResetGame ();
		}
		foreach (FoundationSlot pos in foundationpositions) {
			pos.ResetGame ();
		}
		foreach (FreeCellSlot pos in freecellpositions) {
			pos.ResetGame ();
		}
		GenerateRandomCards ();
		ResetWinConditions ();
	}

	void ResetWinConditions(){
		winConditions = new Dictionary<string,bool> ();
		foreach (string suit in suitMap) {
			winConditions.Add (suit, false);
		}
	}

	public void GenerateRandomCards()
	{
		List<CardData> deck = new List<CardData>();

		Dictionary<string,float> cardWeight = new Dictionary<string, float> ();

		for (int i = 0; i < 13; i++){
			for (int suit = 0; suit < 4; suit++){
				deck.Add(new CardData(suitMap[suit], i+1));
				cardWeight.Add((i+1)+suitMap[suit], Random.Range(0,100f));
			}
		}
		deck = deck.OrderBy(x => cardWeight[x.GetValue() + x.GetSuite()]).ToList();

		int nextSlot = 0;
		foreach(CardData card in deck){
			//for each card
			positions[nextSlot].AddCard(card);
			nextSlot += 1;
			if (nextSlot >= positions.Count)
				nextSlot = 0;
		}
	}

	public void FindSuitableFreeCell(DraggableCard card){
		foreach (FreeCellSlot pos in freecellpositions) {
			pos.CheckPlacable (card);
		}
	}

	public void UpdateWinConditionToTrue(string suit){
		winConditions [suit] = true;
		foreach (bool flag in winConditions.Values) {
			if (flag == false)
				return;
		}
		//do some kind of win thing
	}
}
