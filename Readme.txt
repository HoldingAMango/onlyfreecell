Completed all the functionalities of freecell in the list

Playing area should have:
	4 “free cell” positions at top.
	4 “foundation” position to move cards to complete the game
	8 cascade columns

- The 3 screens:
	o Welcome Screen
	o Play screen
	o End screen
- Functions for the play screen
	o Follow logic/rules of the classic game
	o Random deal to lay out cards in starting cascade columns
	o Clicking on “open” card in a column, will move to open ‘freecell’ if one is available
	o Ability to drag a single card to new legal position
	o Cannot move card to illegal position

Plus - ability to order and move linked suit cards in the cascade columns

I didn't get to finding a good way to use JSON I/O for this project

Also would have prefered to spend more time to make the project better looking
